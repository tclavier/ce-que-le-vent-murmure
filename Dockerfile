FROM registry.gitlab.com/aqoba/docker/hugo:latest
COPY . /src
RUN hugo_env=production hugo --destination=/src/public/
EXPOSE 80
CMD hugo_env=production hugo server --destination=/src/public/ --baseURL http://aqoba --verbose --bind 0.0.0.0 --port 80
