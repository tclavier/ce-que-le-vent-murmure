---
title: "Violence conjugale, le poids des mots"
date: 2017-07-23T18:39:13+02:00
draft: false
tags:
    - violence conjugale
categories:
    - Fenêtres sur le monde
---

Le poids des mots…

Lundi 12 juin, Émilie, 34 ans, mère de 4 enfants, meurt sur les rails d’une ligne TGV, ligotée par son mari dans l’attente du passage du train, en pleine nuit.

Si la méthode utilisée par le criminel est inhabituelle, le traitement de cette actualité par les médias est en revanche plus ordinaire.

Pour une (rare fois), on ne parle ni de crime passionnel, ni de drame conjugal. Les critiques des mouvements féministes auraient-elles été prises en considération ? Rien n’est moins sûr, hélas.

L’affaire est banale : un couple en instance de séparation, un mari qui ne le supporte pas et se transforme en bourreau. Une tragédie classique que les médias nous ressortent régulièrement sous forme de faits-divers. A raison d’une femme succombant tous les 2,7 jours sous les coups de son conjoint ou ex-conjoint et de 223 000 femmes victimes chaque année de violence conjugale, chez FEMEN on parle plutôt de phénomène de société et d’une question de santé publique.

La société et les médias prennent-ils vraiment la mesure de ces crimes ?

L’article s’achève par ce commentaire indulgent : "C'est le geste terrible d'un homme qui a sombré dans une longue dépression", confie une source proche de l'enquête au quotidien. Ni le caractère cruel du procédé ni la préméditation de l’acte ne seront évoqués.

La presse regorge d’articles aux conclusions similaires :

Mai 2016, dans l’Allier : “Un septuagénaire dépressif a abattu son épouse d’un coup de fusil de chasse avant de retourner l’arme contre lui”.

Toujours en mai 2016, dans l’Héraut : “Dépressif, il tue sa femme avant de retourner l’arme contre lui. Selon les premiers éléments de l’enquête, l’homme souffrait de dépression”.

Février 2016 dans le Loiret : “Il tue sa femme et ses enfants et se tranche la gorge : le magistrat n’exclut pas la possibilité d’une dépression”(sic).

Corse, août 2011 : “Dépressif il tue sa femme et ses enfants avant de se donner la mort”.

Les mots féminicide, violence, crime ne sont quasiment jamais employés dans ces contextes pourtant propices.

Que nous dit l’usage répété de ce terme médical dans des histoires de meurtres ? “Dépressif” réoriente notre empathie vers l’auteur du crime et non vers sa/ses victime(s) (laissées dans un flou constant) en en faisant un homme sensible (hum hum), broyé par une destinée irréversible, non responsable de ses actes.

Le choix des mots est primordial, et un ton résolument compatissant vis-à-vis de l’auteur du crime tend à dresser un portrait en creux de la réelle victime.

Changement de ton lorsque l’auteur du crime est une femme (cf Jacqueline Sauvage, Alexandra Lange, Sylvie Leclerc). Aucune hypothétique dépression mise en avant, remise en doute de la version de la femme (notamment au sujet des violences subies par le conjoint). Les articles s’attardent bien plus sur la victime quand il s’agit du conjoint qu’inversement, et là aussi le but (avoué ou inconscient) est de démontrer l’humanité du défunt et démonter l’image négative dressée la suspecte.

Le patriarcat est un système de pensée intégré, c’est pourquoi il est d’autant plus difficile à combattre. Alors que théoriquement, la justice prévoit une égalité de traitement toutes et tous, dans les faits, la perception des affaires diverge selon le sexe de la victime.

La dépression du tueur est un abus de langage tel que “crime passionnel” ou “drame conjugal”, visant non seulement à atténuer la responsabilité du coupable, mais aussi en reportant une partie de cette responsabilité sur les épaules de la victime : si elle n’avait pas quitté son conjoint, il n’aurait pas « sombré » dans la dépression et ne serait jamais passé à l’acte (euphémisme pour carnage familial). Quelque soit son rôle, elle est en partie responsable du drame qui s’est noué. La mort des enfants n’est pas perçue comme l’acte abominable d’un homme violent et possessif (les journaux titrent rarement leurs enfants, mais ses enfants, excluant le lien maternel, comme si avec son départ, elle lui volait ce qui lui appartenait), mais comme l’inéluctable conséquence de l’égoïste décision de la femme.

On nous raconte donc qu’Emilie, 34 ans, mère de quatre enfants, retrouvée coupée en deux sur une ligne de TGV est donc en grande partie l’artisane de son propre malheur. En brisant le cœur de son gentil mari, elle a déclenché une réaction en chaîne incontrôlable.

Le traitement de l’information n’est pas un détail dans la lutte contre le patriarcat. Le rôle des médias est de rendre compte objectivement de faits, non de créer des archétypes qui font perdurer les clichés. Il faut cesser de chercher des excuses aux criminels en avançant des raisons médicales comme des certitudes. La majorité des dépressifs retourne la violence contre eux-mêmes sans entraîner d’autres personnes avec eux

Tuer sa compagne (et parfois ses enfants) n’est pas l’acte d’un dépressif, mais celui d’un homme violent, possessif et immature.

*Publié par FEMEN le  17 juin, 11:35 · sur le site Femen facebook.*
