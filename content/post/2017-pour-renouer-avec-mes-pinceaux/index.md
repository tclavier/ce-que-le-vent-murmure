---
title: Petit retour en arrière ... pour renouer avec mes pinceaux ...
date: 2017-01-16T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
    - pinceaux
    - tsunami
    - vivre
categories:
    - Fenêtres sur le monde
---

Un Tsunami dans ma vie.\
Un effondrement...\
Un arrachement...\
Un déménagement.

Un changement de vie...de tout...

Réapprendre à vivre.

Comment retrouver le goût de rire....mes pinceaux....les couleurs et les mots....


Laisser doucement s'enliser le plomb, revenir ce qui me rend légère...

Me ré-apprivoiser...

Peindre, écouter ce que le vent murmure, laisser son souffle me porter, me ré-alphabétiser, écrire, peindre encore et encore.\
Renaître papillon....

{{< figure src="expo-faure.jpg" title="L'atelier en août, ne pas penser. Peindre." caption="">}}
