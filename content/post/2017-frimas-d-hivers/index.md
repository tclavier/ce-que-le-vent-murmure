---
title: Frimas d'hiver
date: 2017-02-02T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
    - hiver
categories:
    - Fenêtres sur le monde
---

Frimas d'hiver. Le doigt du givre repeint les paysages.\
Cahors à changé de couleur, entre brouillards et gelées blanches, la ville en tenue d'hiver chante encore sous le ciel frileux.

{{< figure src="pont-valentre.png" title="Cahors. Le Pont Valentré en hiver. Le Pont du Diable." caption="Maï Phan Van. Acrylique sur toile 80x90.">}}
