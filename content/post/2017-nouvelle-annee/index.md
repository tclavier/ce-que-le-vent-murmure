---
title: Nouvelle Année... Des clownettes en Ehpad
date: 2017-01-03T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
categories:
    - Fenêtres sur le monde
---

De Noël au 1er Janvier, 2016 aurait glissé tout doucement, au rythme lent de nos anciens...
C'était sans compter avec l'imprévisible des surprises... On rit aussi dans les EHPAD, on vit parfois plus intensément que dans les rues aux vitrines illuminées. Quand les corps se fatiguent, que jambes dos et bras nous lâchent, quand la pensée défaille, il reste encore la lumière des yeux....celle des yeux de l'enfant en nous....

Des improbables visiteuses aux résidents en passant par les membres des équipes soignantes, on ne sait qui au juste à eu le plus de plaisir.
C'est un gage de retour...

{{< figure src="photo-055.jpg" title="Insomnie et Pétunia en visite à L'EHPAD de Luzech(Lot)" caption="">}}
{{< figure src="photo-050.jpg" title="Insomnie et Pétunia en visite à L'EHPAD de Luzech(Lot)" caption="">}}

> "Le rire est le langage des anges, il est ce qui sauve la vie quand les mots ne peuvent plus rien. Il est peut-être bien la chanson préférée de Dieu."

Henri Gougaud, Le rire de la grenouille
