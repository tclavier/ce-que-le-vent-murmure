---
title: Solitude
date: 2017-02-09T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
    - amour
    - douleur
    - infini
    - solitude
    - vie
    - vivre
categories:
    - Fenêtres sur le monde
---

Le temps de l'hiver j'hiberne. \
Enfin autant que faire se peut.\
Nécessité alimentaire oblige.\
Mais dès mon croûton de pain gagné, j'ai hâte de retourner au creux de mon terrier. J'aime ce temps de silence, enveloppé de nuit.\
Il est propice à l'introspection. Il est utile à la création.\
Il est solitude, confrontation à soi-même,\
J'ai fini par l'apprivoiser.\
Il faut avoir traversé des déserts, pleuré des continents de solitude noire, celle de plomb, lourde, pesante et gluante, enduré la solitude terrifiante d'abandon, celle où l'on se meurt, celle où l'on se sent perdu, abandonné, ...peut-être depuis notre origine, arraché de la matrice tiède..\
Il faut l'avoir traversée et puis avoir accepté cette mort là.\
Rendre les armes, Faire allégeance à l'incompréhensible.\
Alors on ressuscite.\
Neuf, neuf au dedans.\
D'autres hivers viendront, et de nouveaux printemps. Désormais on le sait.\
Dans les silences d'hiver, le coeur murmure. Il parle des amours reçus, des graines en attentes, des espérances frémissantes....\
Il parle de l'amour infini qui enveloppe tout être vivant du plus petit cailloux aux plus idylliques créatures, et qui nous est donné sans condition. Lorsque l'on a compris cela, lorsque l'on a senti une fois le souffle de la Vie sur nous, la solitude n'est plus jamais ennemie.\
Alors certains jours elle viendra aimante amante nous emporter sur ses ailes, vers l'infini des bleus du ciel.

"Je crois que pour vivre – parce qu’on peut passer cette vie sans vivre, et c’est un état sans doute pire que la mort – … il faut avoir été regardé au moins une fois, avoir été aimé au moins une fois, avoir été porté au moins une fois. Et après, quand cette chose-là a été donnée, vous pouvez être seul. La solitude n’est plus jamais mauvaise. Même si on ne vous porte plus, même si on ne vous aime plus, même si on ne vous regarde plus, ce qui a été donné, vraiment donné, une fois, l’a été pour toujours. A ce moment-là, vous pouvez aller vers la solitude comme une hirondelle peut aller vers le plein ciel.…" Christian Bobin
