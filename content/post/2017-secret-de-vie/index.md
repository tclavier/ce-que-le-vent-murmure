---
title: Secret de vie
date: 2017-01-17T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
    - amour
    - aurore
    - confiance
    - gratitude
    - lumière
    - naissance
    - naître
    - nuit
    - ombre
    - soleil
    - vie
categories:
    - Fenêtres sur le monde
---

En janvier 2014 je citais Pierre Rabhi :

> "Des songes heureux pour ensemencer les siècles... Sachez que la Création ne nous appartient pas, mais que nous sommes ses enfants. Gardez-vous de toute arrogance car les arbres et toutes les créatures sont également enfants de la Création. Vivez avec légèreté sans jamais outrager l’eau, le souffle ou la lumière. Et si vous prélevez de la vie pour votre vie, ayez de la gratitude. Lorsque vous immolez un animal, sachez que c’est la vie qui se donne à la vie et que rien ne soit dilapidé de ce don. Sachez établir la mesure de toute chose. Ne faites point de bruit inutile, ne tuez pas sans nécessité ou par divertissement. Sachez que les arbres et le vent se délectent de la mélodie qu’ensemble ils enfantent, et l’oiseau, porté par le souffle, est un messager du ciel autant que la terre. Soyez très éveillés lorsque le soleil illumine vos sentiers et lorsque la nuit vous rassemble, ayez confiance en elle, car si vous n’avez ni haine ni ennemi, elle vous conduira sans dommage, sur ses pirogues de silence, jusqu’aux rives de l’aurore. Que le temps et l’âge ne vous accablent pas, car ils vous préparent à d’autres naissances, et dans vos jours amoindris, si votre vie fut juste, il naîtra de nouveaux songes heureux, pour ensemencer les siècles."

Pierre Rabhi, Extrait du Recours à la Terre, Terre du ciel, 1995

http://www.pierrerabhi.org/blog/

Il est bon parfois de relire ses notes... piqûre de rappel, pour faire le point, garder ou réajuster le cap.

Lorsque le soleil illumine vos sentier, n'oubliez pas de rendre grâce à l'univers, goûtez le, savourez le.

Lorsque la nuit vous enveloppe, lorsque le ciel s'assombrit, ayez confiance.\
Lorsque survient une tempête, lorsque la réalité construite autour de vous s'effondre, s'effrite, vole en éclats et se désagrège, ayez confiance. L'ancien doit mourir pour que naisse le nouveau, ne vous agrippez pas à ce qui vous échappe, par habitude, par nostalgie, par peur de l'inconnu qui va venir, par attachement, par désespoir.

N'ayez crainte, si vous n’avez ni haine ni ennemi, cette nuit là, aussi sombre soit-elle, "vous conduira sans dommage, sur ses pirogues de silence, jusqu’aux rives de l’aurore."\
Quelque soit la douleur, aimez la. La douleur est amie, elle est processus de vie, elle travaille en vous. Epousez la. Et puis aimez, aimez, aimez sans retenue le monde, la Vie, les autres, une âme soeur, aimez vous vous même, gardez bras et coeur grands ouverts.\
Recevez avec gratitude tout de la vie, son ombre, sa lumière, il n'y a rien à jeter.

L'aube viendra, ....et l'aurore , ....avec un jour nouveau...et le soleil ...
