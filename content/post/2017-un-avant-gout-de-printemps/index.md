---
title: Un avant goût de printemps
date: 2017-02-20T18:39:13+02:00
author: Maï Phan Van
draft: false
tags:
    - amour
    - humain
    - printemps
    - sensible
    - souffle de vie
    - tendresse
    - vie
categories:
    - Fenêtres sur le monde
---

Violettes, pâquerettes, oiseaux chantants.... un parfum de printemps chatouille les sens dans l'air adouci de soleil.
Dans mon village les jardiniers se réveillent, ça s'agitte dans les jardins.
On taille les branches mortes, on prépare la terre, on désherbe, on bine, on bêche...
Avec amour.
On entourre de tendresse narcisses et jonquilles, on couve d'un regard ému les bourgeons sur le point d'éclore.
Et la brise délicate m'apporte comme une graine portée par le vent cette pensée d'Henri Gougaud:
" Vois les gens comme des jardins, n'y sème pas des grains de mort. "

Alors dans les rayons de ce tendre soleil printanier , les pépiements d'oiseaux , les premiers papillons, les abeilles à nouveau à leur tâche, viennent à moi quelques pensées que j'ai envie de partager comme quelques graines de vie à semer dans les jardins intérieurs que sont nos coeurs:
" Le souffle de vie au plus profond du cœur est « plus petit qu'un grain de riz, qu'un grain d'orge, qu'un grain de moutarde, qu'un grain de mil, plus petit même que le noyau d'un grain de mil, et pourtant plus grand que la terre, que l'espace entre ciel et terre, plus grand que le ciel, plus grand que tous les mondes ». (les Upanishad)
" Ce qui rend effrayants les masques du pouvoir est l'irrémédiable absence d'amour. "
(Henri Gougaud, L'inquisiteur)
"Les poètes déclarent que par le règne de la puissance actuelle, sous le fer de cette gloire, ont surgi les défis qui menacent notre existence sur cette planète ; que, dès lors, tout ce qui existe de sensible de vivant ou d'humain en dessous de notre ciel a le droit, le devoir, de s'en écarter et de concourir d'une manière très humaine, ou d'une autre encore bien plus humaine, à sa disparition." Patrick Chamoiseau (Frères migrants, Déclaration des poètes)

Mystère de la vie, pour germer, toute graine doit mourir en terre afin de révéler ce qu'elle a de plus puissant en elle que l'on ne pouvait soupçonner de l'extérieur. C'est en acceptant de mourir à nous-même que nous pouvons devenir réellement féconds
Être vivant c'est perdre sans cesse, mourir sans cesse, se transformer tout le temps, Grandir en permanence, c'est aussi vieillir, se rider, voir son corps et ses fonctions nous lâcher, comme le signe que nous sommes plus que cela, appelés à autre chose.

Et recevoir avec grâce ce qui advient de nouveau.

{{< figure src="chanter-la-vie.png" title="Chanter la vie" caption="Maï Phan Van. Acrylique sur toile 80x90.">}}
